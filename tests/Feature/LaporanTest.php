<?php

namespace Tests\Feature;

use App\Models\Laporan;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Spatie\Permission\Models\Permission;
use Tests\TestCase;

class LaporanTest extends TestCase
{
   use RefreshDatabase;
    protected function setUp(): void
    {
        // first include all the normal setUp operations
        parent::setUp();

        // now re-register all the roles and permissions (clears cache and reloads relations)
        $this->app->make(\Spatie\Permission\PermissionRegistrar::class)->registerPermissions();

    }

    public function test_guest_can_not_access_laporan_index(): void
    {
        $response = $this->get('/laporan');

        $response->assertStatus(302);
        $response->assertRedirect('login');
    }

    public function test_non_authorized_user_can_not_access_laporan_index():void {
        $user = User::factory()->make();

        $response = $this->actingAs($user)->get('/laporan');

        $response->assertStatus(403);
    }

    public function test_authorized_user_can_access_laporan_index():void {
        $user = User::factory()->create();
        Permission::create(['name' => 'read laporan']);
        $user->givePermissionTo('read laporan');

        $data = [
            'namaPelapor' => 'yus',
            'keterangan' => 'keterangan',
            'alamat' => 'alamat',
            'latitude' => '0',
            'longitude' => '0',
            'noHp' => '0812312312',
            'status' => 1,
            'gambar' => 'laporan.jpg'
        ];
        Laporan::create($data);

        $response = $this->actingAs($user)->get('/laporan');

        $response->assertStatus(200);
        $response->assertViewHas('laporans', function ($laporans)  {
            return $laporans[0]->namaPelapor === 'yus';
        });

    }


    public function test_authorized_user_can_access_create_laporan_page():void {
        $user = User::factory()->create();
        Permission::create(['name' => 'create laporan']);
        $user->givePermissionTo('create laporan');


        $response = $this->actingAs($user)->get('/laporan/create');

        $response->assertStatus(200);
        $response->assertSee('Buat Laporan');
    }

    public function test_authorized_user_can_create_laporan_success():void {
        $user = User::factory()->create();
        Permission::create(['name' => 'create laporan']);
        $user->givePermissionTo('create laporan');
        $data = [
            'namaPelapor' => 'yus',
            'keterangan' => 'keterangan',
            'alamat' => 'alamat',
            'latitude' => '0',
            'longitude' => '0',
            'noHp' => '0812312312',
            'status' => 1,
            'gambar' => UploadedFile::fake()->image('laporan.jpg')
        ];

        $response = $this->actingAs($user)->post('/laporan', $data);

        $response->assertStatus(302);
        $response->assertSessionHasNoErrors();
        $response->assertSessionHas('message');
        $response->assertRedirectToRoute('laporan.index');
        $laporan = Laporan::latest()->first();
        self::assertEquals($laporan->namaPelapor, $data['namaPelapor']);
    }
}
