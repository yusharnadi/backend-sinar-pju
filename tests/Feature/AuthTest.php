<?php

namespace Tests\Feature;


use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use RefreshDatabase;
    public function test_login_response_200(): void
    {
        $response = $this->get('/login');

        $response->assertStatus(200);
    }

    public function test_login_guest_cant_home(){
        $response = $this->get('/');

        $response->assertRedirect('login');
    }

    public function test_login_render_login_view(): void
    {
        $response = $this->get('/login');

        $response->assertViewIs('auth.login');
    }

    public function test_login_action_invalid(): void
    {
        $data = [
            'email' => 'yust.44@gmail.com',
            'password' => '123'
        ];

        $response = $this->post('/login-process', $data);

        $response->assertStatus(302);
        $response->assertSessionHas(['error']);
        $this->assertGuest();
    }

    public function test_login_action_validation_error(): void
    {
        $data = [
            'email' => 'yust.44',
            'password' => ''
        ];

        $response = $this->post('/login-process', $data);

        $response->assertStatus(302);
        $response->assertSessionHasErrors([
            'email',
            'password',
        ]);
    }

    public function test_login_action_success(): void
    {
        $data = [
            'email' => 'yust.44@gmail.com',
            'password' => 'pass'
        ];

        $user = User::factory()->create(['email' => 'yust.44@gmail.com', 'password' => bcrypt('pass')]);

        $response = $this->post('/login-process', $data);

        $response->assertStatus(302);
        $response->assertRedirect('/');
        $this->assertAuthenticatedAs($user);
    }

    public function test_login_authenticated_user_cant_see_login_view(): void
    {
        $user = User::factory()->make(['email' => 'yust.44@gmail.com', 'password' => bcrypt('pass')]);

        $response = $this->actingAs($user)->get('/login');

        $response->assertStatus(302);
        $response->assertRedirect('/');
    }

    public function test_login_logout_success(){
        $user = User::factory()->make();

        $response = $this->actingAs($user)->get('/logout');

        $response->assertStatus(302);
        $this->assertGuest();
    }
}
