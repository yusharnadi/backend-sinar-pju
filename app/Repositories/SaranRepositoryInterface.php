<?php

namespace App\Repositories;

interface SaranRepositoryInterface
{
    public function getAll();
    public function getById(int $id);
    public function insert(array $attributes);
    public function delete(int $id);
}
