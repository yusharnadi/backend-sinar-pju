<?php

namespace App\Repositories\Eloquent;

use App\Models\User;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Support\Facades\DB;

class UserRepository implements UserRepositoryInterface
{
    protected $model;

    public function __construct(User $user)
    {
        $this->model = $user;
    }

    public function getAll()
    {
        return $this->model->all();
        // return DB::table('users')
        // ->join('')
    }

    public function getAllWithRole()
    {
        $result = DB::table('users')
            ->select('users.id', 'users.name', 'users.email', 'roles.name as role')
            ->LeftJoin('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
            ->LeftJoin('roles', 'roles.id', '=', 'model_has_roles.role_id')
            ->get();
        return $result;
    }

    public function getById(int $id)
    {
        return $this->model->findOrFail($id);
    }

    public function insert(array $attributes)
    {
        $user = $this->model->create($attributes);
        $user->assignRole($attributes['role']);
        return $user;
    }

    public function update(int $id, array $attributes)
    {
        unset($attributes['role']);
        $user = $this->model->findOrFail($id);

        $user->update($attributes);
        return $user;
    }

    public function delete(int $id)
    {
        $user = $this->model->findOrFail($id);
        $role = $user->getRoleNames();

        $user->removeRole($role[0]);
        return $user->delete();
    }
}
