<?php

namespace App\Repositories\Eloquent;

use App\Models\Agenda;
use App\Repositories\AgendaRepositoryInterface;

class AgendaRepository implements AgendaRepositoryInterface
{
    protected $model;
    public function __construct(Agenda $model)
    {
        $this->model = $model;
    }

    public function getAll()
    {
        return $this->model->orderBy('id', 'desc')->get();
    }

    public function getLimit(int $limit)
    {
        return $this->model->orderBy('id', 'desc')->limit($limit)->get();
    }

    public function getById(int $id)
    {
        return $this->model->findOrFail($id);
    }

    public function insert(array $attributes)
    {
        return $this->model->create($attributes);
    }

    public function update(int $id, array $attributes)
    {
        $agenda = $this->model->findOrFail($id);
        return $agenda->update($attributes);
    }

    public function delete(int $id)
    {
        $agenda = $this->model->findOrFail($id);
        return $agenda->delete();
    }
}
