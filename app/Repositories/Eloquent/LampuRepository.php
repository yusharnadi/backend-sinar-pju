<?php

namespace App\Repositories\Eloquent;

use App\Models\Lampu;
use App\Repositories\LampuRepositoryInterface;

class LampuRepository implements LampuRepositoryInterface
{
    private $model;

    public function __construct(Lampu $lampu)
    {
        $this->model = $lampu;
    }

    public function getAll()
    {
        return $this->model->orderBy('idLampu', 'desc')->get();
    }

    public function getById(int $id)
    {
        return $this->model->findOrFail($id);
    }

    public function insert(array $attributes)
    {
        return $this->model->create($attributes);
    }

    public function update(int $id, array $attributes)
    {
        $lampu = $this->model->findOrFail($id);

        return $lampu->update($attributes);
    }

    public function delete(int $id)
    {
        $lampu = $this->model->findOrFail($id);
        return $lampu->delete();
    }

    public function countAll()
    {
        return $this->model->count();
    }
}
