<?php

namespace App\Repositories\Eloquent;

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

use App\Repositories\RoleRepositoryInterface;
use Illuminate\Support\Facades\DB;

class RoleRepository implements RoleRepositoryInterface
{
    private $role;
    private $permission;

    public function __construct(Role $role, Permission $permission)
    {
        $this->role = $role;
        $this->permission = $permission;
    }

    public function getAll()
    {
        return $this->role->all();
    }

    public function getPermissionName()
    {
        return $this->permission->all()->pluck('name')->all();
    }

    public function insert(array $attributes)
    {
        $role = $this->role->create(['name' => $attributes['name']]);
        $role->syncPermissions($attributes['permission']);
        return $role;
    }

    public function getById(int $id)
    {
        return $this->role->findOrFail($id);
    }

    public function getUserPermission(int $id)
    {
        return DB::table('role_has_permissions')
            ->where('role_id', $id)
            ->join('permissions', 'permissions.id', '=', 'role_has_permissions.permission_id')
            ->pluck('name')->all();
    }
}
