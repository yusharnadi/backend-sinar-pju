<?php

namespace App\Repositories\Eloquent;

use App\Models\Saran;
use App\Repositories\SaranRepositoryInterface;

class SaranRepository implements SaranRepositoryInterface
{
    protected $model;

    public function __construct(Saran $saran)
    {
        $this->model = $saran;
    }

    public function getAll()
    {
        return $this->model->orderBy('id', 'desc')->get();
    }

    public function getById(int $id)
    {
        return $this->model->findOrFail($id);
    }

    public function insert(array $attributes)
    {
        $saran = $this->model->create($attributes);
        return $saran;
    }


    public function delete(int $id)
    {
        $saran = $this->model->findOrFail($id);
        return $saran->delete();
    }
}
