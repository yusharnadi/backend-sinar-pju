<?php

namespace App\Repositories\Eloquent;

use App\Models\Slider;
use App\Repositories\SliderRepositoryInterface;

class SliderRepository implements SliderRepositoryInterface
{
    public function __construct(protected Slider $model)
    {
    }

    public function getAll()
    {
        return $this->model->orderBy('id', 'desc')->get();
    }

    public function getById(int $id)
    {
        return $this->model->findOrFail($id);
    }

    public function getLimit(int $limit)
    {
        // TODO: Implement getLimit() method.
    }

    public function insert(array $attributes)
    {
        return $this->model->create($attributes);
    }

    public function update(int $id, array $attributes)
    {
        $laporan = $this->model->findOrFail($id);

        return $laporan->update($attributes);
    }

    public function delete(int $id)
    {
        return $this->model->findOrFail($id)->delete();
    }
}
