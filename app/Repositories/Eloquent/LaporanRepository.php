<?php

namespace App\Repositories\Eloquent;

use App\Models\Laporan;
use App\Repositories\LaporanRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class LaporanRepository implements LaporanRepositoryInterface
{
    private $model;

    public function __construct(Laporan $laporan)
    {
        $this->model = $laporan;
    }

    public function getAll()
    {
        return $this->model->orderBy('idLaporan', 'desc')->get();
    }
    public function getAllPaginated()
    {
        return $this->model->orderBy('idLaporan', 'desc')->paginate(10);
    }

    public function getLimit(int $limit)
    {
        return $this->model->orderBy('idLaporan', 'desc')->limit($limit)->get();
    }

    public function getById(int $id)
    {
        return $this->model->findOrFail($id);
    }

    public function insert(array $attributes)
    {
        return $this->model->create($attributes);
    }

    public function update(int $id, array $attributes)
    {
        $laporan = $this->model->findOrFail($id);

        return $laporan->update($attributes);
    }

    public function delete(int $id)
    {
        $laporan = $this->model->findOrFail($id);
        return $laporan->delete();
    }

    public function countAll()
    {
        return $this->model->count();
    }
    public function countByStatus($status)
    {
        return $this->model->where('status', $status)->count();
    }
}
