<?php

namespace App\Repositories\Eloquent;
use App\Models\Petugas;
use \App\Repositories\PetugasRepositoryInterface;
class PetugasRepository implements PetugasRepositoryInterface
{
    protected $model;

    public function __construct(Petugas $petugas)
    {
        $this->model = $petugas;
    }


    public function getAll()
    {
        return $this->model->orderBy('idPetugas', 'desc')->get();
    }

    public function getById(int $id)
    {
        return $this->model->findOrFail($id);
    }

    public function insert(array $attributes)
    {
        return $this->model->create($attributes);
    }

    public function update(int $id, array $attributes)
    {
        $petugas = $this->model->findOrFail($id);
        return $petugas->update($attributes);
    }

    public function delete(int $id)
    {
        $petugas = $this->model->findOrFail($id);
        return $petugas->delete();
    }
}
