<?php

namespace App\Repositories;

interface LaporanRepositoryInterface
{
    public function getAll();
    public function getAllPaginated();
    public function getLimit(int $limit);
    public function getById(int $id);
    public function countAll();
    public function countByStatus($status);
    public function insert(array $attributes);
    public function update(int $id, array $attributes);
    public function delete(int $id);
}
