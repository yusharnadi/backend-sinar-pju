<?php

namespace App\Repositories;

interface UserRepositoryInterface
{
    public function getAll();
    public function getAllWithRole();
    public function getById(int $id);
    public function insert(array $attributes);
    public function update(int $id, array $attributes);
    public function delete(int $id);
}
