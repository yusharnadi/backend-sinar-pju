<?php

namespace App\Repositories;

interface RoleRepositoryInterface
{
    public function getAll();
    public function getById(int $id);
    public function getUserPermission(int $id);
    public function getPermissionName();
    public function insert(array $attributes);
}
