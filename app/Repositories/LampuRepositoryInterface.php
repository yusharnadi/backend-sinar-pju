<?php

namespace App\Repositories;

interface LampuRepositoryInterface
{
    public function getAll();
    public function getById(int $id);
    public function countAll();
    public function insert(array $attributes);
    public function update(int $id, array $attributes);
    public function delete(int $id);
}
