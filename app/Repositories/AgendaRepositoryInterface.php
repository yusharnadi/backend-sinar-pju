<?php

namespace App\Repositories;

interface AgendaRepositoryInterface
{
    public function getAll();
    public function getById(int $id);
    public function getLimit(int $limit);
    public function insert(array $attributes);
    public function update(int $id, array $attributes);
    public function delete(int $id);
}
