<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PetugasStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'namaPetugas' => 'required',
            'nip' => '',
            'email' => 'required|email',
            'noHpPetugas' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:9'
        ];
    }
}
