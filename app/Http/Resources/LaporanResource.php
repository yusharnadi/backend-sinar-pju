<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class LaporanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->idLaporan,
            'namaPelapor' => $this->namaPelapor,
            'keterangan' => $this->keterangan,
            'alamat' => $this->alamat,
            'noHp' => $this->noHp,
            'status' => $this->status,
            'gambar'     => asset('uploads') . '/' . $this->gambar,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
