<?php

namespace App\Http\Controllers;

use App\Http\Requests\PetugasStoreRequest;
use App\Http\Requests\PetugasUpdateRequest;
use App\Services\Petugas\PetugasServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;


class PetugasController extends Controller
{
    protected PetugasServiceInterface $petugasService;

    public function __construct(PetugasServiceInterface $petugasService)
    {
        $this->petugasService = $petugasService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        if (!Auth::user()->can('read petugas')) abort(403);

        $petugas = $this->petugasService->getAll();
        return view('petugas.index', ['petugas' => $petugas]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        if (!Auth::user()->can('create petugas')) abort(403);

        return view('petugas.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PetugasStoreRequest $request)
    {
        if (!Auth::user()->can('create petugas')) abort(403);

        $validatedRequest = $request->safe()->except(['_token']);

        try {
            $this->petugasService->store($validatedRequest);
            return redirect()->route('petugas.index')->with('message', "Berhasil memambahkan petugas.");
        }catch (\Exception $th){
            Log::error($th->getMessage(), ['request' => $validatedRequest]);
            return redirect()->route('petugas.index')->with('error', "Petugas gagal ditambahkan.");
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        if (!Auth::user()->can('update petugas')) abort(403);

        $petugas = $this->petugasService->getById($id);

        return view('petugas.edit', ['petugas' => $petugas]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(PetugasUpdateRequest $request, string $id)
    {
        if (!Auth::user()->can('update petugas')) abort(403);

        $validatedRequest = $request->safe()->except(['_token']);

        try {
            $this->petugasService->update($id, $validatedRequest);
            return redirect()->route('petugas.index')->with('message', "Berhasil mengubah petugas.");
        }catch (\Exception $th){
            Log::error($th->getMessage(), ['request' => $validatedRequest]);
            return redirect()->route('petugas.index')->with('error', "Petugas gagal diubah.");
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if (!Auth::user()->can('delete petugas')) abort(403);

        try {
            $this->petugasService->delete($id);
            return redirect()->route('petugas.index')->with('message', "Petugas berhasil dihapus.");
        } catch (\Exception $th) {
            Log::error($th->getMessage(), ['id petugas' => $id]);
            return redirect()->route('petugas.index')->with('error', "Petugas gagal dihapus.");
        }
    }
}
