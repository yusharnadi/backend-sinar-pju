<?php

namespace App\Http\Controllers;

use App\Http\Requests\SliderStoreRequest;
use App\Services\Slider\SliderServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class SliderController extends Controller
{
    public function __construct(protected SliderServiceInterface $sliderService)
    {
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        if (!Auth::user()->can('read slider')) abort(403);

        $sliders = $this->sliderService->getAll();

        return view('slider.index', ['sliders' => $sliders]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        if (!Auth::user()->can('create slider')) abort(403);

        return view('slider.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(SliderStoreRequest $request)
    {
        if (!Auth::user()->can('create slider')) abort(403);

        $validated_request = $request->safe()->except(['_token']);

        try {
            $this->sliderService->insert($validated_request);
            return redirect()->route('slider.index')->with('message', "Data berhasil ditambahkan.");
        } catch (\Exception $th) {
            Log::error($th->getMessage(), ['request' => $validated_request]);
            return redirect()->route('slider.index')->with('error', "Data gagal ditambahkan.");
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        if (!Auth::user()->can('create slider')) abort(403);

        $slider = $this->sliderService->getById($id);

        return view('slider.edit', ['slider' => $slider]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(SliderStoreRequest $request, string $id)
    {
        if (!Auth::user()->can('update slider')) abort(403);

        $validated_request = $request->safe()->except(['_token']);

        try {
            $this->sliderService->update($id, $validated_request);
            return redirect()->route('slider.index')->with('message', "Data berhasil diubah.");
        } catch (\Exception $th) {
            Log::error($th->getMessage(), ['request' => $validated_request]);
            return redirect()->route('slider.index')->with('error', "Data gagal diubah.");
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if (!Auth::user()->can('delete slider')) abort(403);

        try {
            $this->sliderService->delete($id);
            return redirect()->route('slider.index')->with('message', "Data berhasil dihapus.");
        } catch (\Exception $th) {
            Log::error($th->getMessage(), ['id slider' => $id]);
            return redirect()->route('slider.index')->with('error', "Data gagal dihapus.");
        }
    }
}
