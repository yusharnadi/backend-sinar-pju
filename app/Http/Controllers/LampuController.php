<?php

namespace App\Http\Controllers;

use App\Http\Requests\LampuStoreRequest;
use App\Http\Requests\LampuUpdateRequest;
use App\Services\Lampu\LampuServiceInterface;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class LampuController extends Controller
{
    protected $lampuService;
    protected $jenis_lampu;

    public function __construct(LampuServiceInterface $lampuService)
    {
        $this->lampuService = $lampuService;
        $this->jenis_lampu =  [
            'SON',
            'SON-T',
            'PLC',
            'LED',
        ];;
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if (!Auth::user()->can('read lampu')) abort(403);

        if ($request->ajax()) {
            $lampus = $this->lampuService->getAll();
            return DataTables::of($lampus)
                ->addColumn('action', function ($lampu) {
                    return '
                    <a href="' . route('lampu.edit', $lampu->idLampu) . '" class="btn btn-icon btn-sm btn-warning"><i class="fas fa-edit"></i></a>
                    <a onClick="showModal(' . $lampu->idLampu . ')" data-action="' . route('lampu.destroy', $lampu->idLampu) . '" class="btn-fire btn btn-icon btn-sm btn-danger" href="#"><i class="fas fa-trash"></i></a>
                    ';
                })->toJson();
        }


        return view('lampu.index');
    }

    public function readCsv()
    {
        if (!Auth::user()->can('create lampu')) abort(403);
        $data = [];
        if (($open = fopen(storage_path() . "/pju-fix.csv", "r")) !== FALSE) {

            while (($line = fgetcsv($open, null, ";")) !== FALSE) {
                $data[] = $line;
            }

            fclose($open);
        }

        $lampu = [];
        $counter = 0;
        foreach ($data as $dt) {
            $temp['latitude'] = str_replace('*', '', $dt[0]);
            $temp['longitude'] = str_replace('*', '', $dt[1]);
            $temp['nama'] = $dt[2];
            $temp['daya'] = $dt[3];
            $temp['jenis'] = $dt[4];
            $temp['namaJalan'] = $dt[5];
            $temp['idKwh'] = null;
            $counter++;
            // array_push($lampu, $temp);

            $this->lampuService->store($temp);
        }


        // return response()->json($lampu);
        echo $counter . " was Imported";
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        if (!Auth::user()->can('create lampu')) abort(403);

        return view('lampu.create', ['jenis_lampu' => $this->jenis_lampu]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(LampuStoreRequest $request)
    {
        if (!Auth::user()->can('create lampu')) abort(403);

        $validRequest = $request->safe()->except(['_token']);

        try {
            $this->lampuService->store($validRequest);
            return redirect()->route('lampu.index')->with('message', "Data berhasil ditambahkan.");
        } catch (\Exception $th) {
            Log::error($th->getMessage(), ['request' => $validRequest]);
            return redirect()->route('lampu.index')->with('error', "Data gagal ditambahkan.");
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        if (!Auth::user()->can('update lampu')) abort(403);

        $lampu = $this->lampuService->getById($id);

        return view('lampu.edit', ['lampu' => $lampu, 'jenis_lampu' => $this->jenis_lampu]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(LampuUpdateRequest $request, string $id)
    {
        if (!Auth::user()->can('update lampu')) abort(403);

        $validRequest = $request->safe()->except(['_token']);

        try {
            $this->lampuService->update($id, $validRequest);
            return redirect()->route('lampu.index')->with('message', "Data berhasil diubah.");
        } catch (\Exception $th) {
            Log::error($th->getMessage(), ['request' => $validRequest]);
            return redirect()->route('lampu.index')->with('error', "Data gagal diubah.");
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if (!Auth::user()->can('delete lampu')) abort(403);

        try {
            $this->lampuService->delete($id);
            return redirect()->route('lampu.index')->with('message', "Data berhasil dihapus.");
        } catch (\Exception $th) {
            Log::error($th->getMessage(), ['id lampu' => $id]);
            return redirect()->route('lampu.index')->with('error', "Data gagal dihapus.");
        }
    }
}
