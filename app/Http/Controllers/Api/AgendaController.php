<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LaporanIndexRequest;
use App\Http\Resources\AgendaResource;
use App\Services\Agenda\AgendaServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AgendaController extends Controller
{
    public function __construct(protected AgendaServiceInterface $agendaService)
    {
    }


    public function index(LaporanIndexRequest $request)
    {

        try {

            if (isset($request->limit) && $request->limit != '') {
                $agenda =  AgendaResource::collection($this->agendaService->getLimit($request->limit));
            } else {
                $agenda =  AgendaResource::collection($this->agendaService->getAll());
            }

            return response()->json(
                [
                    'success' => true,
                    'message' => 'Agenda ditemukan.',
                    'data' => $agenda
                ],
                200
            );
        } catch (\Exception $th) {
            Log::error($th->getMessage(), ['context' => 'Failed Get Agenda Index']);
            return response()->json(
                [
                    'success' => false,
                    'message' => $th->getMessage(),
                    'data' => []
                ],
                500
            );
        }
    }

    public function show($id)
    {
        try {
            $agenda = $this->agendaService->getById($id);
            $agendaResponse = new AgendaResource($agenda);

            return response()->json(
                [
                    'success' => true,
                    'message' => 'agenda ditemukan.',
                    'data' => $agendaResponse,
                ],
                200
            );
        } catch (\Exception $th) {
            Log::error($th->getMessage(), ['context' => 'Failed Get agenda detail']);
            return response()->json(
                [
                    'success' => false,
                    'message' => $th->getMessage(),
                    'data' => []
                ],
                500
            );
        }
    }
}
