<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\Lampu\LampuServiceInterface;
use Illuminate\Support\Facades\Log;

use function PHPSTORM_META\type;

class LampuController extends Controller
{
    private $lampuService;

    public function __construct(LampuServiceInterface $lampuService)
    {
        $this->lampuService = $lampuService;
    }

    public function index()
    {
        try {
            $lampu = $this->lampuService->getAll();
            return response()->json(
                [
                    'success' => true,
                    'message' => 'lampu ditemukan.',
                    'data' => $lampu
                ],
                200
            );
        } catch (\Exception $th) {
            Log::error($th->getMessage(), ['context' => 'Failed Get Lampu Index']);
            return response()->json(
                [
                    'success' => false,
                    'message' => $th->getMessage(),
                    'data' => []
                ],
                500
            );
        }
    }

    public function toGeometry()
    {
        $lampu = $this->lampuService->getAll();
        $formated = [];
        foreach ($lampu as $key) {
            $temp = [
                'type' => "Feature",
                "geometry" => [
                    "type" => 'Point',
                    "coordinates" => [(float)$key->longitude, (float)$key->latitude]
                ],
                "properties" => [
                    'id' => $key->idLampu,
                    'nama' => $key->nama,
                    'daya' => $key->daya,
                    'jenis' => $key->jenis,
                    'namaJalan' => $key->namaJalan,
                ]
            ];

            array_push($formated, $temp);
        }

        $geojson = [
            "type" => "FeatureCollection",
            "features" => $formated
        ];
        return response()->json($geojson);
    }
}
