<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\SliderResource;
use App\Services\Slider\SliderServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SliderController extends Controller
{
    public function __construct(protected SliderServiceInterface $sliderService)
    {
    }

    public function index(){
        try {
            $sliders = SliderResource::collection($this->sliderService->getAll());

            return response()->json(
                [
                    'success' => true,
                    'message' => 'Slider ditemukan.',
                    'data' => $sliders
                ], 200
            );
        } catch (\Exception $th) {
            Log::error($th->getMessage(), ['context' => 'Failed Get Slider Index']);
            return response()->json(
                [
                    'success' => false,
                    'message' => $th->getMessage(),
                    'data' => []
                ],
                500
            );
        }
    }
}
