<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\SaranStoreApiRequest;
use App\Services\Saran\SaranServiceInterface;
use Illuminate\Support\Facades\Log;

class SaranController extends Controller
{
    private $saranService;

    public function __construct(SaranServiceInterface $saranService)
    {
        $this->saranService = $saranService;
    }

    public function store(SaranStoreApiRequest $request)
    {
        $token = '17agsts2023';

        $validRequest = $request->safe()->except('token');


        if ($request->token !== $token) {

            return response()->json(
                [
                    'success' => false,
                ],
                403
            );
        }


        try {
            $saran = $this->saranService->create($validRequest);

            return response()->json(
                [
                    'success' => true,
                    'message' => 'Sukses membuat saran.',
                    'data' => $saran
                ],
                200
            );
        } catch (\Exception $th) {
            Log::error($th->getMessage(), ['request' => $validRequest]);
            return response()->json(
                [
                    'success' => false,
                    'message' => $th->getMessage(),
                    'data' => []
                ],
                500
            );
        }
    }
}
