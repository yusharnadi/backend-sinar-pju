<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\LaporanController as ControllersLaporanController;
use App\Http\Requests\LaporanDeleteRequest;
use App\Http\Requests\LaporanIndexRequest;
use App\Http\Requests\LaporanStoreRequest;
use App\Http\Resources\LaporanCollection;
use App\Http\Resources\LaporanResource;
use App\Models\Laporan;
use App\Services\Laporan\LaporanServiceInterface;
use App\Services\Petugas\PetugasServiceInterface;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class LaporanController extends Controller
{
    private $laporanService;

    public function __construct(LaporanServiceInterface $laporanService, private PetugasServiceInterface $petugasService)
    {
        $this->laporanService = $laporanService;
    }

    public function index(LaporanIndexRequest $request)
    {

        try {

            if (isset($request->limit) && $request->limit != '') {
                $laporan =  LaporanResource::collection($this->laporanService->getLimit($request->limit));
            } else {
                $laporan =  new LaporanCollection($this->laporanService->getAllPaginated());
            }

            return response()->json(
                [
                    'success' => true,
                    'message' => 'Laporan ditemukan.',
                    'data' => $laporan
                ],
                200
            );
        } catch (\Exception $th) {
            Log::error($th->getMessage(), ['context' => 'Failed Get Laporan Index']);
            return response()->json(
                [
                    'success' => false,
                    'message' => $th->getMessage(),
                    'data' => []
                ],
                500
            );
        }
    }

    public function store(LaporanStoreRequest $request)
    {
        $validRequest = $request->validated();

        try {
            $token = 'tokenku123987347@1234';

            if ($request->header('token') !== $token) {
                throw new \Exception('Token invalid', 1);
            }

            $laporan = $this->laporanService->store($validRequest);

            return response()->json(
                [
                    'success' => true,
                    'message' => 'Sukses membuat laporan.',
                    'data' => $laporan
                ],
                200
            );
        } catch (\Exception $th) {
            Log::error($th->getMessage(), ['request' => $validRequest]);
            return response()->json(
                [
                    'success' => false,
                    'message' => $th->getMessage(),
                    'data' => []
                ],
                500
            );
        }
    }

    public function show($id)
    {
        try {
            $laporan = $this->laporanService->getById($id);
            $laporanResponse = new  LaporanResource($laporan);
            $petugas = ControllersLaporanController::getPetugas($laporan->petugas, $this->petugasService);

            return response()->json(
                [
                    'success' => true,
                    'message' => 'Laporan ditemukan.',
                    'data' => $laporanResponse,
                    'petugas' => $petugas
                ],
                200
            );
        } catch (\Exception $th) {
            Log::error($th->getMessage(), ['context' => 'Failed Get Laporan detail']);
            return response()->json(
                [
                    'success' => false,
                    'message' => $th->getMessage(),
                    'data' => []
                ],
                500
            );
        }
    }

    public function delete($id, LaporanDeleteRequest $request)
    {
        try {

            $validRequest = $request->validated();

            $laporan = $this->laporanService->getById($id);

            if ($validRequest['noHp'] !== $laporan->noHp) {
                throw new \Exception("No HP tidak cocok dengan laporan.", 1);
            }

            $this->laporanService->delete($id);

            return response()->json(
                [
                    'success' => true,
                    'message' => 'Laporan dihapus.',
                    'data' => []
                ],
                200
            );
        } catch (\Exception $th) {
            Log::error($th->getMessage(), ['context' => 'Failed Delete Laporan']);
            return response()->json(
                [
                    'success' => false,
                    'message' => $th->getMessage(),
                    'data' => []
                ],
                500
            );
        }
    }

    public function getTotalGroupByMonth()
    {


        try {
            $laporan = DB::table('laporans')->select(
                DB::raw("(count(idLaporan)) as total_laporan"),
                DB::raw("(DATE_FORMAT(created_at, '%M')) as month"),
                DB::raw("cast((DATE_FORMAT(created_at, '%c')) as signed)  as bulan")
            )
                ->orderBy('bulan', 'asc')
                ->groupBy('month')
                ->groupBy('bulan')
                ->get();

            return response()->json(
                [
                    'success' => true,
                    'message' => 'Statictic Laporan Calculated.',
                    'data' => $laporan
                ],
                200
            );
        } catch (\Throwable $th) {
            Log::error($th->getMessage(), ['context' => 'Failed Get Stats Laporan']);
            return response()->json(
                [
                    'success' => false,
                    'message' => $th->getMessage(),
                    'data' => []
                ],
                500
            );
        }
    }
}
