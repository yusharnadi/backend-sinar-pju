<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoleStoreRequest;
use App\Services\Role\RoleServiceInterface;
use Illuminate\Support\Facades\Auth;

class RoleController extends Controller
{
    private $roleService;
    public function __construct(RoleServiceInterface $roleService)
    {
        $this->roleService = $roleService;
    }
    public function index()
    {
        if (!Auth::user()->can('read role')) abort(403);


        $roles = $this->roleService->getAll();
        // $roles = Role::all();

        return view('role.index', ['roles' => $roles]);
    }

    public function create()
    {
        if (!Auth::user()->can('create role')) abort(403);

        $permissions = $this->roleService->getPermissionName();

        return view('role/create', ['permissions' => $permissions]);
    }

    public function store(RoleStoreRequest $request)
    {
        if (!Auth::user()->can('create role')) abort(403);

        $validRequest = $request->validated();
        try {
            // $role = Role::create(['name' => $validRequest['name']]);
            // $role->syncPermissions($validRequest['permission']);

            $this->roleService->create($validRequest);
            return redirect()->route('role.index')->with('message', 'Role Created.');
        } catch (\Spatie\Permission\Exceptions\RoleAlreadyExists $e) {
            return redirect()->route('role.index')->with('error', $e->getMessage());
        } catch (Exception $e) {
            return redirect()->route('role.index')->with('error', $e->getMessage());
        }
    }

    public function edit($id)
    {
        if (!Auth::user()->can('update role')) abort(403);

        $role = $this->roleService->getById($id);

        $userPermission = $this->roleService->getUserPermission($id);
        $permissions = $this->roleService->getPermissionName();


        return view('role.edit', [
            'permissions' => $permissions,
            'userPermission' => $userPermission,
            'role' => $role
        ]);
    }

    public function update(RoleStoreRequest $request, $id)
    {
        if (!Auth::user()->can('update role')) abort(403);
        $validRequest = $request->validated();

        try {

            $role = $this->roleService->getById($id);
            $role->syncPermissions($validRequest['permission']);
            return redirect()->route('role.index')->with('message', 'Role and Permission updated.');
        } catch (\Spatie\Permission\Exceptions\PermissionDoesNotExist  $e) {

            return redirect()->route('role.index')->with('error', $e->getMessage());
        } catch (Exception $e) {

            return redirect()->route('role.index')->with('error', $e->getMessage());
        }
    }
}
