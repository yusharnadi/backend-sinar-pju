<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Services\Role\RoleServiceInterface;
use App\Services\User\UserServiceInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    protected $userService;
    protected $roleService;
    public function __construct(UserServiceInterface $userService, RoleServiceInterface $roleService)
    {
        $this->userService = $userService;
        $this->roleService = $roleService;
    }
    /**
     * Display a listing of the resource.
     */


    public function index()
    {
        if (!Auth::user()->can('read user')) abort(403);

        $users = $this->userService->getAllWithRole();
        return view('user.index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        if (!Auth::user()->can('create user')) abort(403);
        $role = $this->roleService->getAll();
        return view('user.create', ['roles' => $role]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(UserStoreRequest $request)
    {
        if (!Auth::user()->can('create user')) abort(403);

        $validRequest = $request->safe()->except(['_token']);
        $validRequest['password'] = Hash::make($validRequest['password']);

        try {
            $this->userService->store($validRequest);
            return redirect()->route('users.index')->with('message', "User berhasil dibuat.");
        } catch (\Exception $th) {
            Log::error($th->getMessage(), ['request' => $validRequest]);
            return redirect()->route('users.index')->with('error', "User gagal dibuat.");
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        if (!Auth::user()->can('create user')) abort(403);

        $roles = $this->roleService->getAll();
        $users = $this->userService->getById($id);
        $role = $users->getRoleNames();
        // dd($role[0]);
        return view('user.edit', ['roles' => $roles, 'user' => $users, 'role' => $role]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UserUpdateRequest $request, string $id)
    {
        if (!Auth::user()->can('update user')) abort(403);

        $validRequest = $request->safe()->except(['_token']);

        if ($request->has('password') && $request->password != '') {
            $validRequest['password'] = Hash::make($validRequest['password']);
        }

        // dd($validRequest);

        try {
            $this->userService->update($id, $validRequest);
            return redirect()->route('users.index')->with('message', "User berhasil diubah.");
        } catch (\Exception $th) {
            Log::error($th->getMessage(), ['request' => $validRequest]);
            return redirect()->route('users.index')->with('error', "User gagal diubah.");
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function delete(int $id)
    {
        if (!Auth::user()->can('delete user')) abort(403);

        try {
            $this->userService->delete($id);
            return redirect()->route('users.index')->with('message', "User berhasil dihapus.");
        } catch (\Exception $th) {
            Log::error($th->getMessage(), ['user_id' => $id]);
            return redirect()->route('users.index')->with('error', "User gagal dihapus.");
        }
    }
}
