<?php

namespace App\Http\Controllers;

use App\Http\Requests\SaranStoreRequest;
use App\Services\Saran\SaranServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class SaranController extends Controller
{
    protected $saranService;

    public function __construct(SaranServiceInterface $saranService)
    {
        $this->saranService = $saranService;
    }
    public function index()
    {
        if (!Auth::user()->can('read saran')) abort(403);

        $sarans = $this->saranService->getAll();
        return view('saran.index', ['sarans' => $sarans]);
    }

    public function create()
    {
        if (!Auth::user()->can('create saran')) abort(403);

        return view('saran.create');
    }

    public function store(SaranStoreRequest $request)
    {
        if (!Auth::user()->can('create saran')) abort(403);

        $validRequest = $request->safe()->except(['_token']);

        try {
            $this->saranService->create($validRequest);
            return redirect()->route('saran.index')->with('message', "Saran berhasil dibuat.");
        } catch (\Exception $th) {
            Log::error($th->getMessage(), ['request' => $validRequest]);
            return redirect()->route('saran.index')->with('error', "Saran gagal dibuat.");
        }
    }

    public function delete(int $id)
    {
        if (!Auth::user()->can('delete saran')) abort(403);

        try {
            $this->saranService->delete($id);
            return redirect()->route('saran.index')->with('message', "Saran berhasil dihapus.");
        } catch (\Exception $th) {
            Log::error($th->getMessage(), ['id_saran' => $id]);
            return redirect()->route('saran.index')->with('error', "Saran gagal dihapus.");
        }
    }
}
