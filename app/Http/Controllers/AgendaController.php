<?php

namespace App\Http\Controllers;

use App\Http\Requests\AgendaStoreRequest;
use App\Http\Requests\AgendaUpdateRequest;
use App\Services\Agenda\AgendaServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class AgendaController extends Controller
{
    protected AgendaServiceInterface $agendaService ;

    /**
     * @param AgendaServiceInterface $agendaService
     */
    public function __construct(AgendaServiceInterface $agendaService)
    {
        $this->agendaService = $agendaService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        if (!Auth::user()->can('read agenda')) abort(403);
        $agendas = $this->agendaService->getAll();
        return view('agenda.index', ['agendas' => $agendas]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        if (!Auth::user()->can('create agenda')) abort(403);
        return view('agenda.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(AgendaStoreRequest $request)
    {
        if (!Auth::user()->can('create agenda')) abort(403);
        $validatedRequest = $request->safe()->except(['_token']);

        try {
            $this->agendaService->insert($validatedRequest);
            return redirect()->route('agenda.index')->with('message', "Agenda berhasil dibuat.");
        } catch (\Exception $th) {
            Log::error($th->getMessage(), ['request' => $validatedRequest]);
            return redirect()->route('agenda.index')->with('error', "Agenda gagal dibuat.");
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        if (!Auth::user()->can('update agenda')) abort(403);

        $agenda = $this->agendaService->getById($id);
        return view('agenda.edit', ['agenda' => $agenda]);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(AgendaUpdateRequest $request, string $id)
    {
        if (!Auth::user()->can('update agenda')) abort(403);
        $validatedRequest = $request->safe()->except(['_token']);
        try {
            $this->agendaService->update($id, $validatedRequest);
            return redirect()->route('agenda.index')->with('message', "Agenda berhasil diubah.");
        } catch (\Exception $th) {
            Log::error($th->getMessage(), ['request' => $validatedRequest]);
            return redirect()->route('agenda.index')->with('error', "Agenda gagal diubah.");
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if (!Auth::user()->can('delete agenda')) abort(403);

        try {
            $this->agendaService->delete($id);
            return redirect()->route('agenda.index')->with('message', "Agenda berhasil dihapus.");
        } catch (\Exception $th) {
            Log::error($th->getMessage(), ['id Agenda' => $id]);
            return redirect()->route('agenda.index')->with('error', "Agenda gagal dihapus.");
        }

    }
}
