<?php

namespace App\Http\Controllers;

use App\Services\Lampu\LampuServiceInterface;
use App\Services\Laporan\LaporanServiceInterface;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    protected $laporanService;
    protected $lampuService;

    public function __construct(LaporanServiceInterface $laporanService, LampuServiceInterface $lampuService)
    {
        $this->laporanService = $laporanService;
        $this->lampuService = $lampuService;
    }
    public function index()
    {
        $laporan = $this->laporanService->getLimit(5);
        $total_laporan = $this->laporanService->countAll();
        $total_laporan_diproses = $this->laporanService->countByStatus(2);
        $total_laporan_selesai = $this->laporanService->countByStatus(3);
        $total_lampu = $this->lampuService->countAll();


        $compactData = [
            'laporans' => $laporan,
            'total_laporan' => $total_laporan,
            'total_laporan_diproses' => $total_laporan_diproses,
            'total_laporan_selesai' => $total_laporan_selesai,
            'total_lampu' => $total_lampu,
        ];
        return view('dashboard', $compactData);
    }
}
