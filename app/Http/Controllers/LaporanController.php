<?php

namespace App\Http\Controllers;

use App\Http\Requests\LaporanStoreRequest;
use App\Http\Requests\LaporanUpdateRequest;
use App\Services\Laporan\LaporanServiceInterface;
use App\Services\Petugas\PetugasServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class LaporanController extends Controller
{
    private $laporanService;
    private $petugasService;

    public function __construct(LaporanServiceInterface $laporanService, PetugasServiceInterface $petugasService)
    {
        $this->laporanService = $laporanService;
        $this->petugasService = $petugasService;
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        if (!Auth::user()->can('read laporan')) abort(403);

        $laporans = $this->laporanService->getAll();
        return view('laporan.index', ['laporans' => $laporans]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        if (!Auth::user()->can('create laporan')) abort(403);

        return view('laporan.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(LaporanStoreRequest $request)
    {
        if (!Auth::user()->can('create laporan')) abort(403);

        $validRequest = $request->safe()->except(['_token']);

        try {
            $this->laporanService->store($validRequest);
            return redirect()->route('laporan.index')->with('message', "Laporan berhasil dibuat.");
        } catch (\Exception $th) {
            Log::error($th->getMessage(), ['request' => $validRequest]);
            return redirect()->route('laporan.index')->with('error', "Laporan gagal dibuat.");
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        if (!Auth::user()->can('read laporan')) abort(403);

        $laporan = $this->laporanService->getById($id);

        $petugas = self::getPetugas($laporan->petugas, $this->petugasService);

        return view('laporan.detail', ['laporan' => $laporan, 'petugas' => $petugas]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        if (!Auth::user()->can('update laporan')) abort(403);

        $laporan = $this->laporanService->getById($id);
        $petugas = $this->petugasService->getAll();

        return view('laporan.edit', ['laporan' => $laporan, 'petugas' => $petugas]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function updateImage(LaporanUpdateRequest $request, string $id)
    {
        if (!Auth::user()->can('update laporan')) abort(403);

        $validRequest = $request->safe()->except(['_token']);
        // dd($validRequest);
        try {
            $this->laporanService->update($id, $validRequest);
            return redirect()->route('laporan.index')->with('message', "Laporan berhasil diubah.");
        } catch (\Exception $th) {
            Log::error($th->getMessage(), ['request' => $validRequest]);
            return redirect()->route('laporan.index')->with('error', "Laporan gagal diubah.");
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if (!Auth::user()->can('delete laporan')) abort(403);

        try {
            $this->laporanService->delete($id);
            return redirect()->route('laporan.index')->with('message', "Laporan berhasil dihapus.");
        } catch (\Exception $th) {
            Log::error($th->getMessage(), ['id Laporan' => $id]);
            return redirect()->route('laporan.index')->with('error', "Laporan gagal dihapus.");
        }
    }

    public static function getPetugas(?string $petugasId,  PetugasServiceInterface $petugasService)
    {
        $petugas = [];

        if ($petugasId && $petugasId !== '' && $petugasId !== null) {
            $petugasId = json_decode($petugasId);

            for ($i = 0; $i < count($petugasId); $i++) {
                $petugas_row = $petugasService->getById($petugasId[$i]);
                $petugas[] = $petugas_row;
            }
        }

        return $petugas;
    }
}
