<?php

namespace App\Services\Agenda;

use App\Repositories\AgendaRepositoryInterface;

class AgendaService implements AgendaServiceInterface
{
    protected AgendaRepositoryInterface $agendaRepository;

    /**
     * @param AgendaRepositoryInterface $agendaRepository
     */
    public function __construct(AgendaRepositoryInterface $agendaRepository)
    {
        $this->agendaRepository = $agendaRepository;
    }


    public function getAll()
    {
        return $this->agendaRepository->getAll();
    }

    public function getLimit(int $limit)
    {
        return $this->agendaRepository->getLimit($limit);
    }

    public function getById(int $id)
    {
        return $this->agendaRepository->getById($id);
    }

    public function insert(array $attributes)
    {
        $attributes['foto'] = uploadFile($attributes['foto']);
        $agenda = $this->agendaRepository->insert($attributes);

        if (!$agenda) {
            deleteFile($attributes['foto']);
            throw new \Exception("Error Processing to DB (Agenda Service)", 1);
        }

        return $agenda;
    }

    public function update(int $id, array $attributes)
    {
        if (array_key_exists('foto', $attributes)) {
            $oldAgenda = $this->agendaRepository->getById($id);

            // upload file baru
            $attributes['foto'] =  uploadFile($attributes['foto']);

            //delete file lama
            deleteFile($oldAgenda->foto);
        }

        return $this->agendaRepository->update($id, $attributes);
    }

    public function delete(int $id)
    {
        $agenda = $this->agendaRepository->getById($id);

        //delete file lama
        deleteFile($agenda->foto);
        return $this->agendaRepository->delete($id);
    }
}
