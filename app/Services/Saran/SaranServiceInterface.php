<?php

namespace App\Services\Saran;

interface SaranServiceInterface
{
    public function getAll();
    public function create(array $attributes);
    public function getById($id);
    public function delete($id);
}
