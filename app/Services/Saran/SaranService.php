<?php

namespace App\Services\Saran;

use App\Repositories\SaranRepositoryInterface;

class SaranService implements SaranServiceInterface
{
    protected $saranRepository;
    public function __construct(SaranRepositoryInterface $saranRepositoryInterface)
    {
        $this->saranRepository = $saranRepositoryInterface;
    }
    public function getAll()
    {
        return $this->saranRepository->getAll();
    }
    public function getById($id)
    {
    }
    public function create(array $attributes)
    {
        return $this->saranRepository->insert($attributes);
    }
    public function delete($id)
    {
        return $this->saranRepository->delete($id);
    }
}
