<?php

namespace App\Services\Lampu;

use App\Repositories\LampuRepositoryInterface;

class LampuService implements LampuServiceInterface
{
    protected $lampuRepository;

    public function __construct(LampuRepositoryInterface $lampuRepository)
    {
        $this->lampuRepository = $lampuRepository;
    }

    public function getAll()
    {
        return $this->lampuRepository->getAll();
    }

    public function getById(int $id)
    {
        return $this->lampuRepository->getById($id);
    }

    public function store(array $attributes)
    {
        return $this->lampuRepository->insert($attributes);
    }

    public function update(int $id, array $attributes)
    {
        return $this->lampuRepository->update($id, $attributes);
    }

    public function delete(int $id)
    {
        return $this->lampuRepository->delete($id);
    }

    public function countAll()
    {
        return $this->lampuRepository->countAll();
    }
}
