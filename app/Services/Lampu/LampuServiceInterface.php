<?php

namespace App\Services\Lampu;

interface LampuServiceInterface
{
    public function getAll();
    public function getById(int $id);
    public function countAll();
    public function store(array $attributes);
    public function update(int $id, array $attributes);
    public function delete(int $id);
}
