<?php

namespace App\Services\Laporan;

use App\Repositories\LaporanRepositoryInterface;

class LaporanService implements LaporanServiceInterface
{
    private $laporanRepository;
    public function __construct(LaporanRepositoryInterface $laporanRepository)
    {
        $this->laporanRepository = $laporanRepository;
    }

    public function getAll()
    {
        return $this->laporanRepository->getAll();
    }

    public function getAllPaginated()
    {
        return $this->laporanRepository->getAllPaginated();
    }

    public function getLimit(int $limit)
    {
        return $this->laporanRepository->getLimit($limit);
    }

    public function getById(int $id)
    {
        return $this->laporanRepository->getById($id);
    }
    public function store(array $attributes)
    {
        $attributes['gambar'] = uploadFile($attributes['gambar']);
        $laporan = $this->laporanRepository->insert($attributes);

        if (!$laporan) {
            deleteFile($attributes['gambar']);
            throw new \Exception("Error Processing to DB (Laporan Service)", 1);
        }

        return $laporan;
    }
    public function update(int $id, array $attributes)
    {

        if (array_key_exists('gambar', $attributes)) {
            $oldLaporan = $this->laporanRepository->getById($id);

            // upload file baru 
            $attributes['gambar'] =  uploadFile($attributes['gambar']);

            //delete file lama 
            deleteFile($oldLaporan->gambar);
        }

        return $this->laporanRepository->update($id, $attributes);
    }

    public function delete(int $id)
    {
        $laporan = $this->laporanRepository->getById($id);

        //delete file lama 
        deleteFile($laporan->gambar);

        return $this->laporanRepository->delete($id);
    }

    public function countAll()
    {
        return $this->laporanRepository->countAll();
    }

    public function countByStatus($status)
    {
        return $this->laporanRepository->countByStatus($status);
    }
}
