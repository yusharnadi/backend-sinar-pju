<?php

namespace App\Services\Laporan;

interface LaporanServiceInterface
{
    public function getAll();
    public function getAllPaginated();
    public function countAll();
    public function countByStatus($status);
    public function getById(int $id);
    public function getLimit(int $limit);
    public function store(array $attributes);
    public function update(int $id, array $attributes);
    public function delete(int $id);
}
