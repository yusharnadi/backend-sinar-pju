<?php

namespace App\Services\Role;

use App\Repositories\RoleRepositoryInterface;

class RoleService implements RoleServiceInterface
{
    private $roleRepository;

    public function __construct(RoleRepositoryInterface $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }
    public function getAll()
    {
        return $this->roleRepository->getAll();
    }
    public function getPermissionName()
    {
        return $this->roleRepository->getPermissionName();
    }
    public function create(array $attributes)
    {
        return $this->roleRepository->insert($attributes);
    }
    public function getById(int $id)
    {
        return $this->roleRepository->getById($id);
    }

    public function getUserPermission(int $id)
    {
        return $this->roleRepository->getUserPermission($id);
    }
}
