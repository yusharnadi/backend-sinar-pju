<?php

namespace App\Services\Role;

interface RoleServiceInterface
{
    public function getAll();
    public function getPermissionName();
    public function getById(int $id);
    public function getUserPermission(int $id);
    public function create(array $attributes);
}
