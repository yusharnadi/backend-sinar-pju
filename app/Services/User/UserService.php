<?php

namespace App\Services\User;

use App\Repositories\UserRepositoryInterface;

class UserService implements UserServiceInterface
{
    private $userRepository;
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }
    public function getAll()
    {
        return $this->userRepository->getAll();
    }

    public function getAllWithRole()
    {
        return $this->userRepository->getAllWithRole();
    }

    public function store(array $attributes)
    {
        return $this->userRepository->insert($attributes);
    }

    public function getById(int $id)
    {
        return $this->userRepository->getById($id);
    }

    public function update(int $id, array $attributes)
    {
        $user = $this->userRepository->update($id, $attributes);
        $user->syncRoles($attributes['role']);
    }

    public function delete(int $id)
    {
        return $this->userRepository->delete($id);
    }
}
