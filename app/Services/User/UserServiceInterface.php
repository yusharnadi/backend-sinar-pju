<?php

namespace App\Services\User;

interface UserServiceInterface
{
    public function getAll();
    public function getAllWithRole();
    public function getById(int $id);
    public function store(array $attributes);
    public function update(int $id, array $attributes);
    public function delete(int $id);
}
