<?php

namespace App\Services\Slider;

use App\Repositories\SliderRepositoryInterface;

class SliderService implements SliderServiceInterface
{
    public function __construct(protected SliderRepositoryInterface $sliderRepository)
    {
    }

    public function getAll()
    {
        return $this->sliderRepository->getAll();
    }

    public function getById(int $id)
    {
        return $this->sliderRepository->getById($id);
    }

    public function getLimit(int $limit)
    {
        return $this->sliderRepository->getLimit($limit);
    }

    public function insert(array $attributes)
    {
        $attributes['gambar'] = uploadFile($attributes['gambar']);
        $laporan = $this->sliderRepository->insert($attributes);

        if (!$laporan) {
            deleteFile($attributes['gambar']);
            throw new \Exception("Error Processing to DB (Laporan Service)", 1);
        }

        return $laporan;
    }

    public function update(int $id, array $attributes)
    {
        if (array_key_exists('gambar', $attributes)) {
            $oldLaporan = $this->sliderRepository->getById($id);

            // upload file baru
            $attributes['gambar'] =  uploadFile($attributes['gambar']);

            //delete file lama
            deleteFile($oldLaporan->gambar);
        }

        return $this->sliderRepository->update($id, $attributes);
    }

    public function delete(int $id)
    {
        $slider = $this->sliderRepository->getById($id);

        //delete file lama
        deleteFile($slider->gambar);

        return $this->sliderRepository->delete($id);
    }
}
