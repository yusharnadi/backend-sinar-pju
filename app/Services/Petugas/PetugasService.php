<?php

namespace App\Services\Petugas;

use App\Repositories\PetugasRepositoryInterface;

class PetugasService implements PetugasServiceInterface
{
    protected PetugasRepositoryInterface $petugasRepository;
    public function __construct(PetugasRepositoryInterface $petugasRepository)
    {
        $this->petugasRepository = $petugasRepository;
    }

    public function getAll()
    {
        return $this->petugasRepository->getAll();
    }

    public function getById(int $id)
    {
        return $this->petugasRepository->getById($id);
    }

    public function store(array $attributes)
    {
        return $this->petugasRepository->insert($attributes);
    }

    public function update(int $id, array $attributes)
    {
        return $this->petugasRepository->update($id, $attributes);
    }

    public function delete(int $id)
    {
        return $this->petugasRepository->delete($id);
    }
}
