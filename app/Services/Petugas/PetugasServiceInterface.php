<?php

namespace App\Services\Petugas;

interface PetugasServiceInterface
{
    public function getAll();
    public function getById(int $id);
    public function store(array $attributes);
    public function update(int $id, array $attributes);
    public function delete(int $id);
}
