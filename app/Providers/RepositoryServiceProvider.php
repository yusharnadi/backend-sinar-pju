<?php

namespace App\Providers;

use App\Repositories\AgendaRepositoryInterface;
use App\Repositories\Eloquent\AgendaRepository;
use App\Repositories\Eloquent\LampuRepository;
use App\Repositories\Eloquent\LaporanRepository;
use App\Repositories\Eloquent\PetugasRepository;
use App\Repositories\Eloquent\RoleRepository;
use App\Repositories\Eloquent\SaranRepository;
use App\Repositories\Eloquent\SliderRepository;
use App\Repositories\Eloquent\UserRepository;
use App\Repositories\LampuRepositoryInterface;
use App\Repositories\LaporanRepositoryInterface;
use App\Repositories\PetugasRepositoryInterface;
use App\Repositories\RoleRepositoryInterface;
use App\Repositories\SaranRepositoryInterface;
use App\Repositories\SliderRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use App\Services\Agenda\AgendaService;
use App\Services\Agenda\AgendaServiceInterface;
use App\Services\Petugas\PetugasService;
use App\Services\Petugas\PetugasServiceInterface;
use App\Services\Saran\SaranService;
use App\Services\Saran\SaranServiceInterface;
use App\Services\Lampu\LampuService;
use App\Services\Lampu\LampuServiceInterface;
use App\Services\Laporan\LaporanService;
use App\Services\Laporan\LaporanServiceInterface;
use App\Services\Role\RoleService;
use App\Services\Role\RoleServiceInterface;
use App\Services\Slider\SliderService;
use App\Services\Slider\SliderServiceInterface;
use App\Services\User\UserService;
use App\Services\User\UserServiceInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(UserServiceInterface::class, UserService::class);
        $this->app->bind(RoleRepositoryInterface::class, RoleRepository::class);
        $this->app->bind(RoleServiceInterface::class, RoleService::class);
        $this->app->bind(LaporanRepositoryInterface::class, LaporanRepository::class);
        $this->app->bind(LaporanServiceInterface::class, LaporanService::class);
        $this->app->bind(LampuRepositoryInterface::class, LampuRepository::class);
        $this->app->bind(LampuServiceInterface::class, LampuService::class);
        $this->app->bind(SaranRepositoryInterface::class, SaranRepository::class);
        $this->app->bind(SaranServiceInterface::class, SaranService::class);
        $this->app->bind(PetugasRepositoryInterface::class, PetugasRepository::class);
        $this->app->bind(PetugasServiceInterface::class, PetugasService::class);
        $this->app->bind(AgendaRepositoryInterface::class, AgendaRepository::class);
        $this->app->bind(AgendaServiceInterface::class, AgendaService::class);
        $this->app->bind(SliderServiceInterface::class, SliderService::class);
        $this->app->bind(SliderRepositoryInterface::class, SliderRepository::class);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
