@extends('layouts.admin-master')
@section('page-title', 'Edit Data Lampu')
@section('page-heading')
    <h1>Edit Data Lampu</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item"><a href="{{route('lampu.index')}}">Lampu</a></div>
        <div class="breadcrumb-item">Edit Data Lampu</div>
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @if ($errors->any())
                        @foreach ($errors->all() as $err)
                            <div class="alert alert-danger">
                                {{$err}}
                            </div>
                        @endforeach
                    @endif
                    <form action="{{route('lampu.update', $lampu->idLampu)}}" class="form" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group row mb-2">
                            <label for="nama" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama</label>
                            <div class="col-sm-12 col-md-2">
                                <input type="text" id="nama" name="nama" class="form-control" value="{{old('nama') ?? $lampu->nama}}" required>
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label for="longitude" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Longitude</label>
                            <div class="col-sm-12 col-md-2">
                                <input type="text" id="longitude" name="longitude" class="form-control" value="{{old('longitude') ?? $lampu->longitude}}" required>
                                <small id="longitudeHelp" class="form-text text-muted">
                                    contoh: 108.9975500
                                </small>
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label for="latitude" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Latitude</label>
                            <div class="col-sm-12 col-md-2">
                                <input type="text" id="latitude" name="latitude" class="form-control" value="{{old('latitude') ?? $lampu->latitude}}" required>
                                <small id="latitudeHelp" class="form-text text-muted">
                                    contoh: 0.909819966
                                </small>
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label for="jenis" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jenis Lampu</label>
                            <div class="col-sm-12 col-md-2">
                                <select id="jenis" name="jenis" class="form-control select2" required>
                                    <option value="">--- Pilih Jenis ---</option>
                                    @foreach ($jenis_lampu as $item)
                                        <option value="{{$item}}" @selected($lampu->jenis === $item)>{{$item}}</option>    
                                    @endforeach
                                </select>
                            </div>
                          </div>
                        <div class="form-group row mb-2">
                            <label for="daya" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Daya</label>
                            <div class="col-sm-12 col-md-2">
                                <input type="text" id="daya" name="daya" class="form-control" value="{{old('daya') ?? $lampu->daya}}" required>
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label for="namaJalan" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Jalan</label>
                            <div class="col-sm-12 col-md-4">
                                <input type="text" id="namaJalan" name="namaJalan" class="form-control" value="{{old('namaJalan') ?? $lampu->namaJalan}}" required>
                            </div>
                        </div>


                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection


