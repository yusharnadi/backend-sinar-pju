@extends('layouts.admin-master')
@section('page-title', 'Lampu')
@section('page-heading')
  <h1>Lampu</h1>
@endsection
@section('content')
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body table-responsive">
          <a href="{{route('lampu.create')}}" class="btn btn-primary mb-2">Tambah Data</a>
          @if (session('error'))
            <div class="alert alert-danger">{{session('error')}}</div> 
          @endif
          @if (session('message'))
            <div class="alert alert-info">{{session('message')}}</div> 
          @endif
          <table class="table table-hover datatable">
            <thead>
              <tr>
                <th style="width: 30px">ID#</th>
                <th>Nama</th>
                <th>Latitude</th>
                <th>Longitude</th>
                <th>Daya</th>
                <th>Jenis</th>
                <th>Nama Jalan</th>
                <th style="width: 60px">action</th>
              </tr>
            </thead>
            <tbody>
            {{-- @foreach ($lampus as $lampu)  
                <tr>
                    <td>{{$lampu->idLampu}}</td>
                    <td>{{$lampu->nama}}</td>
                    <td>{{$lampu->latitude}}</td>
                    <td>{{$lampu->longitude}}</td>
                    <td>{{$lampu->daya}}</td>
                    <td>{{$lampu->jenis}}</td>
                    <td>{{$lampu->namaJalan}}</td>
                    <td>
                        <a href="{{route('lampu.show', $lampu->idLampu)}}" class="btn btn-icon btn-sm btn-info"><i class="fas fa-eye"></i></a>
                        <a href="{{route('lampu.edit', $lampu->idLampu)}}" class="btn btn-icon btn-sm btn-warning"><i class="fas fa-edit"></i></a>
                        <a data-toggles="modal" id="smallButton" data-target="#deleteModal" data-action="{{route('lampu.destroy', $lampu->idLampu)}}" class="btn btn-icon btn-sm btn-danger btn-fire" href="#"><i class="fas fa-trash"></i></a>
                    </td>
                </tr>
            @endforeach --}}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

@endsection
@push('modal')
    <!-- Modal -->
  <div class="modal fade" tabindex="-1" role="dialog" id="deleteModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form action="" method="POST" id="formDelete">
          @csrf
          @method('DELETE')
        <div class="modal-header">
          <h5 class="modal-title">Konfirmasi Hapus Data</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Apakah Anda yakin menghapus data ? </p>
        </div>
        <div class="modal-footer bg-whitesmoke br">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-danger">Save changes</button>
        </div>
      </form>
      </div>
    </div>
  </div>
@endpush

@push('page-css')
  <link rel="stylesheet" href="{{asset('assets/modules/DataTables/dt/css/dataTables.bootstrap4.min.css')}}">
@endpush

@push('page-js')
  <script src="{{asset('assets/modules/DataTables/dt/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('assets/modules/DataTables/dt/js/dataTables.bootstrap4.min.js')}}"></script>
  <script>
    $(document).ready( function () {
      $('.datatable').DataTable(
        {
          ajax: '{{route('lampu.index')}}',
          serverSide: true,
          columns : [
            {data: 'idLampu', name: 'idLampu'},
            {data: 'nama', name: 'nama'},
            {data: 'latitude', name: 'latitude'},
            {data: 'longitude', name: 'longitude'},
            {data: 'daya', name: 'daya'},
            {data: 'jenis', name: 'jenis'},
            {data: 'namaJalan', name: 'namaJalan'},
            {data: 'action', name: 'action', orderable: false},
          ],
          order: [[0, 'desc']]
        }
      );

      
      $('#deleteModal').on('hidden.bs.modal', function (e) {
        $('#formDelete').attr('action', '')
      })

    });

    var showModal = function(id){
      console.log(id);
      $('#deleteModal').modal({ backdrop: 'static', keyboard: false })
      $('#formDelete').attr('action', "{{route('lampu.index')}}/" + id)
    }
  </script>
@endpush
