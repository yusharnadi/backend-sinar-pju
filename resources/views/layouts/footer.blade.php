<footer class="main-footer">
    <div class="footer-left">
      Copyright &copy; 2023 <div class="bullet"></div> {{config('app.name')}} <a href="{{route('dashboard')}}" target="_blank"> | DINAS PERHUBUNGAN KOTA SINGKAWANG</a>
    </div>
    <div class="footer-right">
      V 1.0
    </div>
</footer>