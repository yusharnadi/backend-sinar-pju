<div class="main-sidebar">
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand">
      <a href="{{route('dashboard')}}"><img class="mr-1" src="{{asset('assets/img/logo-dishub.png')}}" alt="Logo Pemkot Singkawang" srcset="" height="50px">
        {{config('app.name')}}
      </a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="{{route('dashboard')}}">
        <img src="{{asset('assets/img/logo-dishub.png')}}" alt="Logo Pemkot" srcset="" height="50px">
      </a>
    </div>
    <ul class="sidebar-menu">
      <li class="{{set_active('dashboard')}}"><a class="nav-link" href="{{ route('dashboard') }}"><i class="fas fa-laptop"></i><span>Dashboard</span></a></li>

      @can("read user")
      <li class="menu-header">Transaction Data</li>
      @endcan

      @can("read laporan")
      <li class="{{set_active('laporan.*')}}"><a class="nav-link" href="{{ route('laporan.index') }}"><i class="fas fa-bullhorn"></i><span>Laporan</span></a></li>
      @endcan

      @can("read saran")
      <li class="{{set_active('saran.*')}}"><a class="nav-link" href="{{ route('saran.index') }}"><i class="far fa-comments"></i><span>Masukan & Saran</span></a></li>
      @endcan

        @can("read agenda")
            <li class="{{set_active('agenda.*')}}"><a class="nav-link" href="{{ route('agenda.index') }}"><i class="far fa-calendar-alt"></i><span>Agenda</span></a></li>
        @endcan


      @can("read user")
      <li class="menu-header">Master Data</li>
      @endcan

      @can("read lampu")
      <li class="{{set_active('lampu.*')}}"><a class="nav-link" href="{{ route('lampu.index') }}"><i class="far fa-lightbulb"></i><span>Lampu</span></a></li>
      @endcan

      @can("read petugas")
      <li class="{{set_active('petugas.*')}}"><a class="nav-link" href="{{ route('petugas.index') }}"><i class="fas fa-street-view"></i><span>petugas</span></a></li>
      @endcan

        @can("read slider")
            <li class="{{set_active('slider.*')}}"><a class="nav-link" href="{{ route('slider.index') }}"><i class="far fa-images"></i><span>Slider</span></a></li>
        @endcan
      @can("read user")
      <li class="{{set_active('users.*')}}"><a class="nav-link" href="{{ route('users.index') }}"><i class="far fa-user"></i><span>Users</span></a></li>
      @endcan

      @can("read role")
      <li class="{{set_active('role.*')}}"><a class="nav-link" href="{{ route('role.index') }}"><i class="fas fa-user-tag"></i><span>Role</span></a></li>
      @endcan
    </ul>
    <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
      <a href="{{ route('logout') }}" class="btn btn-danger btn-lg btn-block btn-icon-split">
        <i class="fas fa-sign-out-alt"></i> Logout
      </a>
    </div>
  </aside>
</div>
