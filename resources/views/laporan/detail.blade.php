@extends('layouts.admin-master')
@section('page-title', 'Detail Laporan')
@section('page-heading')
  <h1>Detail Laporan</h1>
  <div class="section-header-breadcrumb">
    <div class="breadcrumb-item"><a href="{{route('laporan.index')}}">Laporan</a></div>
    <div class="breadcrumb-item">Detail Laporan</div>
  </div>
@endsection
@section('content')
<div class="row">
  <div class="col-12 col-md-6 col-lg-6">
    <div class="card card-primary">
        <div class="card-header">
            <h4>Data Pelapor</h4>
        </div>
        <div class="card-body">
            <div class="form-group row">
                <label class="col-form-label text-md-left col-12">Nama Pelapor</label>
                <div class="col-12 col-xl-6">
                    <input type="text" class="form-control" value="{{$laporan->namaPelapor}}" readonly>
                  </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label text-md-left col-12">No Handphone</label>
                <div class="col-12 col-xl-6">
                    <input type="text" class="form-control" value="{{$laporan->noHp}}" readonly>
                  </div>
            </div>
        </div>
    </div>

    <div class="card card-primary">
        <div class="card-header">
            <h4>Detail Laporan</h4>
        </div>
        <div class="card-body">
            <div class="form-group row">
                <label class="col-form-label text-md-left col-12">Keterangan</label>
                <div class="col-12">
                    <textarea class="form-control" style="height: 60px;" readonly>{{$laporan->keterangan}}</textarea>
                  </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label text-md-left col-12 ">Alamat</label>
                <div class="col-12">
                    <textarea class="form-control" style="height: 60px;" readonly>{{$laporan->alamat}}</textarea>
                  </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label text-md-left col-12 col-md-2 col-lg-2">Status</label>
                <div class="col-sm-12 col-md-8 col-lg-8">
                    @if ($laporan->status == 1)
                        <div class="badge badge-info">Ditinjau</div>
                    @elseif($laporan->status == 2)
                        <div class="badge badge-primary">Diproses</div>
                    @else
                        <div class="badge badge-success">Selesai</div>
                    @endif
                  </div> 
            </div>
            <div class="form-group row">
                <label for="" class="col-form-label text-md-left col-12 col-md-2 col-lg-2">Petugas</label>
                <div class="col-sm-2 col-md-8 col-lg-8">   
                    <ul class="list-group list-group-flush">
                        @foreach ($petugas as $p)
                            <li class="list-group-item"><a href="#" class="font-weight-600 mb-4"><img src="{{asset('assets')}}/img/avatar/avatar-1.png" alt="avatar" width="30" class="rounded-circle mr-1"> {{$p->nip .' - '.$p->namaPetugas}}</a></li>
                        @endforeach
                      </ul>
                </div>
            </div>
            <div id='map' style='width: 100%; height: 300px;'></div>
            <a href="{{url()->previous()}}" class="btn btn-icon btn-danger mt-4"><i class="fa fa-arrow-left"></i> Kembali</a>
        </div>
    </div>
  </div>

  <div class="col-12 col-md-6 col-lg-6">
    <div class="card card-primary">
        <div class="card-header">
            <h4>Gambar Laporan</h4>
        </div>
        <div class="card-body">
            <img src="{{asset('uploads'). '/' .$laporan->gambar}}" class="img-fluid img-thumbnail mb-4" alt="{{$laporan->idLaporan}}">
        </div>
    </div>
  </div>
</div>
@endsection

@push('page-css') 
    <link href='https://api.mapbox.com/mapbox-gl-js/v2.15.0/mapbox-gl.css' rel='stylesheet' />
@endpush

@push('page-js')
    <script src='https://api.mapbox.com/mapbox-gl-js/v2.15.0/mapbox-gl.js'></script>  
    <script>
        mapboxgl.accessToken = 'pk.eyJ1IjoieXVzaGFybmFkaSIsImEiOiJjbGp3YzVpeWgwb2FiM2luMXQwNjkzMW0yIn0.3CXh3GgHOJCbUhZgEyrAYA';
        const map = new mapboxgl.Map({
        container: 'map', // container ID
        style: 'mapbox://styles/mapbox/streets-v12', // style URL
        center: [{{$laporan->longitude .',' . $laporan->latitude }}], // starting position [lng, lat]
        zoom: 14, // starting zoom
        });

        // ADD USER PIN LOCATION 
        map.addControl(new mapboxgl.GeolocateControl({
            positionOptions: {
                enableHighAccuracy: true
            },
            trackUserLocation: true,
            showUserHeading: true
        }
        ));

        // ADD ZOOM BUTTON 
        const nav = new mapboxgl.NavigationControl({
            visualizePitch: true
        });

        map.addControl(nav, 'bottom-right');

        // Set marker options.
        const marker = new mapboxgl.Marker({
        color: "#F06292",
        draggable: true
        }).setLngLat([{{$laporan->longitude .',' . $laporan->latitude }}])
        .addTo(map);
        </script>
@endpush

