@extends('layouts.admin-master')
@section('page-title', 'Buat Laporan')
@section('page-heading')
  <h1>Buat laporan</h1>
  <div class="section-header-breadcrumb">
    <div class="breadcrumb-item"><a href="{{route('laporan.index')}}">Laporan</a></div>
    <div class="breadcrumb-item">Buat Laporan</div>
  </div>
@endsection
@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
          @if ($errors->any())
            @foreach ($errors->all() as $err)
              <div class="alert alert-danger">
                  {{$err}}
              </div>
            @endforeach
          @endif
        <form action="{{route('laporan.store')}}" class="form" method="POST" enctype="multipart/form-data">
          @csrf
          <div class="form-group row mb-2">
            <label for="namaPelapor" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Pelapor</label>
            <div class="col-sm-12 col-md-2">
              <input type="text" id="namaPelapor" name="namaPelapor" class="form-control" value="{{old('namaPelapor')}}" required>
            </div>
          </div>
          <div class="form-group row mb-2">
            <label for="keterangan" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Keterangan</label>
            <div class="col-sm-12 col-md-4">
                <textarea name="keterangan" id="keterangan" class="form-control" style="height: 80px">{{old('keterangan')}}</textarea>
            </div>
          </div>
          <div class="form-group row mb-2">
            <label for="alamat" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">alamat</label>
            <div class="col-sm-12 col-md-4">
                <textarea name="alamat" id="alamat" class="form-control" style="height: 80px">{{old('alamat')}}</textarea>
            </div>
          </div>
          <div class="form-group row mb-2">
            <label for="latitude" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Latitude</label>
            <div class="col-sm-12 col-md-2">
              <input type="text" id="latitude" name="latitude" class="form-control" value="{{old('latitude') ?? 0}}" required>
            </div>
          </div>
          <div class="form-group row mb-2">
            <label for="longitude" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Longitude</label>
            <div class="col-sm-12 col-md-2">
              <input type="text" id="longitude" name="longitude" class="form-control" value="{{old('longitude') ?? 0}}" required>
            </div>
          </div>
          <div class="form-group row mb-2">
            <label for="noHp" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">No HP</label>
            <div class="col-sm-12 col-md-2">
              <input type="text" id="noHp" name="noHp" class="form-control" value="{{old('noHp')}}" required>
            </div>
          </div>
          <div class="form-group row mb-2">
            <label for="status" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Status</label>
            <div class="col-sm-12 col-md-2">
                <select id="status" name="status" class="form-control select2" required>
                    <option value="">--- Pilih Status ---</option>
                    <option value="1" selected>Ditinjau (masuk)</option>
                    <option value="2">Diproses</option>
                    <option value="3">Selesai</option>
                </select>
            </div>
          </div>
          <div class="form-group row mb-2">
            <label for="gambar" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Gambar</label>
            <div class="col-sm-12 col-md-2">
              <input type="file" id="gambar" name="gambar" class="form-control" value="{{old('gambar')}}" required>
            </div>
          </div>
          <div class="form-group row mb-4">
            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
            <div class="col-sm-12 col-md-7">
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </div>
        </form>
        <a href="{{url()->previous()}}" class="btn btn-icon btn-danger"><i class="fa fa-arrow-left"></i> Kembali</a>
      </div>
    </div>
  </div>
</div>
@endsection

@push('page-css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@push('page-js')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
  // In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.select2').select2();
});
</script>
@endpush

