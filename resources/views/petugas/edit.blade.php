@extends('layouts.admin-master')
@section('page-title', 'Edit Petugas')
@section('page-heading')
    <h1>Edit Petugas</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item"><a href="{{route('petugas.index')}}">Petugas</a></div>
        <div class="breadcrumb-item">Edit Petugas</div>
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @if ($errors->any())
                        @foreach ($errors->all() as $err)
                            <div class="alert alert-danger">
                                {{$err}}
                            </div>
                        @endforeach
                    @endif
                    <form action="{{route('petugas.update', $petugas->idPetugas)}}" class="form" method="POST">
                        @method('PUT')
                        @csrf
                        <div class="form-group row mb-2">
                            <label for="nip" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">NIP</label>
                            <div class="col-sm-12 col-md-4">
                                <input type="text" id="nip" name="nip" class="form-control" value="{{old('nip') ?? $petugas->nip}}" required>
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label for="namaPetugas" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Petugas</label>
                            <div class="col-sm-12 col-md-4">
                                <input type="text" id="namaPetugas" name="namaPetugas" class="form-control" value="{{old('namaPetugas') ?? $petugas->namaPetugas}}" required>
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label for="email" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Email</label>
                            <div class="col-sm-12 col-md-4">
                                <input type="email" id="email" name="email" class="form-control" value="{{old('email') ?? $petugas->email}}" required>
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label for="noHpPetugas" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">No HP</label>
                            <div class="col-sm-12 col-md-4">
                                <input type="text" id="noHpPetugas" name="noHpPetugas" class="form-control" value="{{old('noHpPetugas') ?? $petugas->noHpPetugas}}" required>
                            </div>
                        </div>


                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection


