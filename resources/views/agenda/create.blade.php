@extends('layouts.admin-master')
@section('page-title', 'Tambah Agenda')
@section('page-heading')
    <h1>Tambah Agenda</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item"><a href="{{route('saran.index')}}">Agenda</a></div>
        <div class="breadcrumb-item">Tambah Agenda</div>
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @if ($errors->any())
                        @foreach ($errors->all() as $err)
                            <div class="alert alert-danger">
                                {{$err}}
                            </div>
                        @endforeach
                    @endif
                    <form action="{{route('agenda.store')}}" class="form" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row mb-2">
                            <label for="judul" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Judul Agenda</label>
                            <div class="col-sm-12 col-md-6">
                                <input type="text" id="judul" name="judul" class="form-control" value="{{old('judul')}}" required>
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label for="deskripsi" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Deskripsi Agenda</label>
                            <div class="col-sm-12 col-md-4">
                                <textarea class="form-control" name="deskripsi" id="deskripsi" style="height: 300px">{{old('deskripsi')}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label for="foto" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Foto Agenda</label>
                            <div class="col-sm-12 col-md-2">
                                <input type="file" id="foto" name="foto" class="form-control" value="{{old('foto')}}" required>
                            </div>
                        </div>


                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection


