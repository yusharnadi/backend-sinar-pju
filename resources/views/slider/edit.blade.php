@extends('layouts.admin-master')
@section('page-title', 'Ubah Slider')
@section('page-heading')
    <h1>Ubah Slider</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item"><a href="{{route('slider.index')}}">Slider</a></div>
        <div class="breadcrumb-item">Ubah Slider</div>
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @if ($errors->any())
                        @foreach ($errors->all() as $err)
                            <div class="alert alert-danger">
                                {{$err}}
                            </div>
                        @endforeach
                    @endif
                    <form action="{{route('slider.update', $slider->id)}}" class="form" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group row mb-2">
                            <label for="nama" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama</label>
                            <div class="col-sm-12 col-md-2">
                                <input type="text" id="nama" name="nama" class="form-control" value="{{old('nama') ?? $slider->nama}}" required>
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label for="gambar" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Gambar</label>
                            <div class="col-sm-12 col-md-3">
                                <input type="file" id="gambar" name="gambar" class="form-control" value="{{old('gambar')}}" required>
                                <small id="gambarHelp" class="form-text text-muted">
                                    Ratio ukuran gambar harus 16/9 (640x360)
                                </small>
                                <img src="{{asset('uploads/'.$slider->gambar)}}" class="img-preview img-thumbnail mt-2" alt="null">
                            </div>
                        </div>

                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('page-js')
    <script>


        $('#gambar').change(function(){
            console.log('firel');
            let reader = new FileReader();

            reader.onload = (e) => {

                $('.img-preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(this.files[0]);
        })
    </script>
@endpush

