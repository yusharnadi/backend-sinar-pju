@extends('layouts.admin-master')
@section('page-title', 'Tambah Saran')
@section('page-heading')
  <h1>Tambah Saran</h1>
  <div class="section-header-breadcrumb">
    <div class="breadcrumb-item"><a href="{{route('saran.index')}}">Masukan & Saran</a></div>
    <div class="breadcrumb-item">Tambah Masukan & Saran</div>
  </div>
@endsection
@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
          @if ($errors->any())
            @foreach ($errors->all() as $err)
              <div class="alert alert-danger">
                  {{$err}}
              </div>
            @endforeach    
          @endif
        <form action="{{route('saran.store')}}" class="form" method="POST">
          @csrf
          <div class="form-group row mb-2">
            <label for="nama" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama</label>
            <div class="col-sm-12 col-md-4">
              <input type="text" id="nama" name="nama" class="form-control" value="{{old('nama')}}" required>
            </div>
          </div>
          <div class="form-group row mb-2">
            <label for="saran" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Masukan & Saran</label>
            <div class="col-sm-12 col-md-4">
                <textarea name="saran" id="saran" class="form-control" style="height: 100px" required></textarea>
            </div>
          </div>

          <div class="form-group row mb-4">
            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
            <div class="col-sm-12 col-md-7">
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection


