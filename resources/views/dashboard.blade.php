@extends('layouts.admin-master')
@section('page-title', 'Dashboard')
@section('page-heading')
  <h1>Dashboard</h1>
  <div class="section-header-breadcrumb">
  </div>
@endsection

@section('content')
<div class="row">
  <div class="col-lg-3 col-md-6 col-sm-6 col-12">
    <div class="card card-statistic-1">
      <div class="card-icon bg-primary">
        <i class="far fa-lightbulb"></i>
      </div>
      <div class="card-wrap">
        <div class="card-header">
          <h4>Total Lampu</h4>
        </div>
        <div class="card-body">
          {{$total_lampu}}
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-md-6 col-sm-6 col-12">
    <div class="card card-statistic-1">
      <div class="card-icon bg-info">
        <i class="fas fa-bullhorn"></i>
      </div>
      <div class="card-wrap">
        <div class="card-header">
          <h4>Total Laporan</h4>
        </div>
        <div class="card-body">
          {{$total_laporan}}
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-md-6 col-sm-6 col-12">
    <div class="card card-statistic-1">
      <div class="card-icon bg-success">
        <i class="fas fa-clipboard-check"></i>
      </div>
      <div class="card-wrap">
        <div class="card-header">
          <h4>Laporan Selesai</h4>
        </div>
        <div class="card-body">
          {{$total_laporan_selesai}}
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-md-6 col-sm-6 col-12">
    <div class="card card-statistic-1">
      <div class="card-icon bg-warning">
        <i class="fas fa-tools"></i>
      </div>
      <div class="card-wrap">
        <div class="card-header">
          <h4>Laporan sdg diproses</h4>
        </div>
        <div class="card-body">
          {{$total_laporan_diproses}}
        </div>
      </div>
    </div>
  </div>                  
</div>
{{-- STATISTIK & LAPORAN TERBARU --}}
<div class="row">
  <div class="col-lg-8 col-md-12 col-sm-12 col-12">
    <div class="card">
        <div class="card-header">
          <h4>Informasi</h4>
        </div>
        <div class="card-body">
          {{-- <div class="alert alert-warning alert-has-icon">
            <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
            <div class="alert-body">
              <div class="alert-title">Pengumuman !</div>
              Lorem ipsum dolor sit, amet consectetur adipisicing elit. Commodi nam pariatur in praesentium optio sint ab suscipit exercitationem eos dolorum facilis qui ipsa molestiae aliquam enim voluptatem architecto, at error quo veritatis? Est facere ducimus, odio laboriosam cum libero vel praesentium reprehenderit illo doloribus sit
              </div>
          </div>
          <ul>
            <li>Lorem, ipsum dolor sit amet consectetur adipisicing elit.</li>
            <li>Lorem ipsum dolor sit amet consectetur adipisicing elit.</li>
            <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt.</li>
            <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt.</li>
          </ul> --}}
          <canvas id="chart" width="100%" height="50px">
            <p>Hello Fallback World</p>
        </canvas>
        </div>
      </div>
  </div> 
  <div class="col-lg-4 col-md-12 col-12 col-sm-12">
    <div class="card">
      <div class="card-header">
        <h4>Laporan Terbaru</h4>
      </div>
      <div class="card-body">             
        <ul class="list-unstyled list-unstyled-border">
          @foreach ($laporans as $laporan)
          <li class="media">
            @if ($laporan->status == 1)
              <div class="badge badge-pill mr-3 badge-info float-right">Ditinjau</div>
            @elseif($laporan->status == 2)
                <div class="badge badge-pill mr-3 badge-primary float-right">Diproses</div>
            @else
                <div class="badge badge-pill mr-3 badge-success float-right">Selesai</div>
            @endif
            <div class="media-body">
              <div class="float-right text-primary text-small">{{$laporan->created_at}}</div>
              <div class="media-title">{{$laporan->namaPelapor}}</div>
              <span class="text-small text-muted">{{$laporan->keterangan}}</span>
            </div>
          </li>
          @endforeach
        </ul>
        <div class="text-center pt-1 pb-1">
          <a href="{{route('laporan.index')}}" class="btn btn-primary btn-lg btn-round">
            Lihat Semua
          </a>
        </div>
      </div>
    </div>
  </div>               
</div>
{{-- PETA PJU  --}}
<div class="row">
  <div class="col col-lg-12">
    <div class="card">
      <div class="card-header"><h4>Pemetaan LAMPU PJU</h4></div>
      <div class="card-body">
        <div id="map" style='width: 100%; height: 700px;'></div>
      </div>
    </div>
  </div>  
</div>
@endsection

@push('page-css') 
    <link href='https://api.mapbox.com/mapbox-gl-js/v2.15.0/mapbox-gl.css' rel='stylesheet' />
@endpush
@push('page-js')
    <script src="https://cdn.jsdelivr.net/npm/chart.js@4.3.2/dist/chart.umd.min.js"></script>    
    <script src='https://api.mapbox.com/mapbox-gl-js/v2.15.0/mapbox-gl.js'></script>  
    <script>
        mapboxgl.accessToken = 'pk.eyJ1IjoieXVzaGFybmFkaSIsImEiOiJjbGp3YzVpeWgwb2FiM2luMXQwNjkzMW0yIn0.3CXh3GgHOJCbUhZgEyrAYA';
        const map = new mapboxgl.Map({
        container: 'map', // container ID
        style: 'mapbox://styles/mapbox/streets-v12', // style URL
        center: [108.988788, 0.910595], // starting position [lng, lat]
        zoom: 17, // starting zoom
        });

        map.loadImage('{{asset('assets/img/pju.png')}}', (error, image) => {
          if (error) throw error;
          if (!map.hasImage('pju')) map.addImage('pju', image);
        });


        // ADD dataSource to MAP 
        map.on('load', () => {
          map.addSource('lampu', {
          'type': 'geojson',
          'data': '{{route('lampu.geometry')}}'
          });

          map.addLayer({
          'id': 'lampu',
          'type': 'symbol',
          'source': 'lampu',
          'layout': {
            // 'icon-image': 'fuel',
            'icon-size': 0.3,
            'icon-image': 'pju',
            'icon-allow-overlap': false
          }
          });
      });

        // logging features for a specific layer (with `e.features`)
        map.on('click', 'lampu', (e) => {
          // Copy coordinates array.
          const coordinates = e.features[0].geometry.coordinates.slice();
          const prop = e.features[0].properties;
          
          // Ensure that if the map is zoomed out such that multiple
          // copies of the feature are visible, the popup appears
          // over the copy being pointed to.
          while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
          coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
        }
      
        new mapboxgl.Popup()
            .setLngLat(coordinates)
            .setHTML(`<h5>Nama : ${prop.nama}</h5><br><p>Daya : ${prop.daya} Type: ${prop.jenis}</p><p>${prop.namaJalan}</p>`)
            .addTo(map);
        });
            // Change the cursor to a pointer when the mouse is over the places layer.
        map.on('mouseenter', 'lampu', () => {
            map.getCanvas().style.cursor = 'pointer';
        });
        
        // Change it back to a pointer when it leaves.
        map.on('mouseleave', 'lampu', () => {
          map.getCanvas().style.cursor = '';
        });


        // ADD USER PIN LOCATION 
        map.addControl(new mapboxgl.GeolocateControl({
            positionOptions: {
                enableHighAccuracy: true
            },
            trackUserLocation: true,
            showUserHeading: true
        }
        ));

        // ADD ZOOM BUTTON 
        const nav = new mapboxgl.NavigationControl({
            visualizePitch: true
        });

        map.addControl(nav, 'bottom-right');
        map.addControl(new mapboxgl.FullscreenControl());

        </script>



        {{-- CHART JS  --}}
        <script>
        
          $(function(){

              const CHART_COLORS = {
              green: 'rgb(75, 192, 192)',
              blue: 'rgb(54, 162, 235)',
            };

            const BG_COLORS = {
              green: 'rgba(75, 192, 192,0.5)',
              blue: 'rgba(54, 162, 235,0.5)',
            };

            const data = {
              labels: [],
              datasets: [
                {
                  label: 'Total Laporan',
                  data: [],
                  borderColor: CHART_COLORS.blue,
                  backgroundColor: BG_COLORS.blue,
                },
                // {
                //   label: 'Laporan Selesai',
                //   data: [],
                //   borderColor: CHART_COLORS.green,
                //   backgroundColor: BG_COLORS.green,
                // }
              ]
            };
          
            const config = {
              type: 'bar',
              data: data,
              options: {
                responsive: true,
                plugins: {
                  legend: {
                    position: 'top',
                  },
                  title: {
                    display: true,
                    text: 'Grafik Total Laporan per Bulan'
                  }
                },
                scales: {
                  y: {
                    beginAtZero: true,
                  }
              },
              },
            };
          
            const myChart = new Chart("chart",config);

            function getTotalByMonth(){
              // call ajax 
              $.ajax({
                url: '{{route('laporan.totalMonth')}}',
                method: 'get',
                cache: false
              })
              .done(function(data){
                if(data.success && data.data.length > 0 ){
                  data.data.forEach(function(index){
                    myChart.data.labels.push(index.month);
                    myChart.data.datasets[0].data.push(index.total_laporan);
                  })
                }

                myChart.update();
              })
              .fail(function(){
                alert('Gagal menampilkan data grafik');
              })
            }

          getTotalByMonth();


          })


        </script>
@endpush


