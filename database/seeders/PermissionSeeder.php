<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions

        Permission::create(['name' => 'read user']);
        Permission::create(['name' => 'create user']);
        Permission::create(['name' => 'update user']);
        Permission::create(['name' => 'delete user']);

        Permission::create(['name' => 'read role']);
        Permission::create(['name' => 'create role']);
        Permission::create(['name' => 'update role']);
        Permission::create(['name' => 'delete role']);

        Permission::create(['name' => 'read laporan']);
        Permission::create(['name' => 'create laporan']);
        Permission::create(['name' => 'update laporan']);
        Permission::create(['name' => 'delete laporan']);

        Permission::create(['name' => 'read lampu']);
        Permission::create(['name' => 'create lampu']);
        Permission::create(['name' => 'update lampu']);
        Permission::create(['name' => 'delete lampu']);

        Permission::create(['name' => 'read kwh']);
        Permission::create(['name' => 'create kwh']);
        Permission::create(['name' => 'update kwh']);
        Permission::create(['name' => 'delete kwh']);

        Permission::create(['name' => 'read slider']);
        Permission::create(['name' => 'create slider']);
        Permission::create(['name' => 'update slider']);
        Permission::create(['name' => 'delete slider']);

        Permission::create(['name' => 'read saran']);
        Permission::create(['name' => 'create saran']);
        Permission::create(['name' => 'update saran']);
        Permission::create(['name' => 'delete saran']);

        Permission::create(['name' => 'read agenda']);
        Permission::create(['name' => 'create agenda']);
        Permission::create(['name' => 'update agenda']);
        Permission::create(['name' => 'delete agenda']);

        Permission::create(['name' => 'read petugas']);
        Permission::create(['name' => 'create petugas']);
        Permission::create(['name' => 'update petugas']);
        Permission::create(['name' => 'delete petugas']);


        // create roles and assign created permissions
        // this can be done as separate statements


        $pimpinan = Role::create(['name' => 'Pimpinan']);
        $pimpinan->syncPermissions(
            [
                'read slider',
                'read laporan',
                'read lampu',
                'read kwh',
                'read saran',
                'read agenda',
                'read petugas',
            ]
        );

        $qa = Role::create(['name' => 'Operator']);
        $qa->syncPermissions(
            [
                'read lampu', 'create lampu', 'update lampu',
                'read laporan', 'create laporan', 'update laporan',
                'read slider', 'create slider', 'update slider',
                'read saran', 'create saran', 'update saran',
                'read agenda', 'create agenda', 'update agenda',
                'read petugas', 'create petugas', 'update petugas',
                'read user',
                'read role'
            ]
        );

        $role = Role::create(['name' => 'Administrator']);
        $role->givePermissionTo(Permission::all());
    }
}
