<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $user = new User();
        $user->name = "administrator";
        $user->email = "yust.44@gmail.com";
        $user->password = Hash::make('Mimi#080810');
        $user->save();
        $user->assignRole('Administrator');

        $opd = new User();
        $opd->name = "operator";
        $opd->email = "op@dishub.com";
        $opd->password = Hash::make('op#123456');
        $opd->save();
        $opd->assignRole('Operator');
    }
}
