<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('lampus', function (Blueprint $table) {
            $table->id('idLampu');
            $table->integer('idKwh')->nullable();
            $table->string('nama');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('jenis');
            $table->string('daya');
            $table->string('namaJalan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('lampus');
    }
};
