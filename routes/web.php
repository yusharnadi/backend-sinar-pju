<?php

use App\Http\Controllers\AgendaController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\LampuController;
use App\Http\Controllers\LaporanController;
use App\Http\Controllers\PetugasController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\SaranController;
use App\Http\Controllers\SliderController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::middleware('guest')->group(function () {
    Route::get('/login', [AuthController::class, 'login'])->name('login');
    Route::post('/login-process', [AuthController::class, 'loginProcess'])->name('login.process');
});

Route::get('/privacy', function () {
    return view('privacy');
});

Route::middleware('auth')->group(function () {
    Route::get('/', [DashboardController::class, 'index'])->name('dashboard');

    Route::resource('/users', UserController::class);
    Route::resource('/laporan', LaporanController::class);
    Route::resource('/lampu', LampuController::class);
    Route::resource('/petugas', PetugasController::class);
    Route::resource('/agenda', AgendaController::class);
    Route::resource('/slider', SliderController::class);



    Route::get('/role', [RoleController::class, 'index'])->name('role.index');
    Route::get('/role/create', [RoleController::class, 'create'])->name('role.create');
    Route::post('/role', [RoleController::class, 'store'])->name('role.store');
    Route::get('/role/{id}/edit', [RoleController::class, 'edit'])->name('role.edit');
    Route::put('/role/{id}/update', [RoleController::class, 'update'])->name('role.update');

    Route::get('/saran', [SaranController::class, 'index'])->name('saran.index');
    Route::get('/saran/create', [SaranController::class, 'create'])->name('saran.create');
    Route::post('/saran', [SaranController::class, 'store'])->name('saran.store');
    Route::get('/saran/{id}/update', [SaranController::class, 'delete'])->name('role.delete');



    // DELETE ROUTE
    Route::get('/user/{id}/delete', [UserController::class, 'delete'])->name('users.delete');
    Route::get('/saran/{id}/delete', [SaranController::class, 'delete'])->name('saran.delete');
    //     Route::get('/lampu/read/file', [LampuController::class, 'readCsv'])->name('lampu.read');
    // Route::get('/laporan/{id}/delete', [LaporanController::class, 'delete'])->name('laporan.delete');

    // CUSTOM ROUTER FOR PUT / PATCH IMAGE FORM
    Route::post('/laporan/{id}/updateImage', [LaporanController::class, 'updateImage'])->name('laporan.updateImage');


    Route::get('/logout', [AuthController::class, 'logout'])->name('logout');
});
