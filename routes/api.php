<?php

use App\Http\Controllers\Api\AgendaController;
use App\Http\Controllers\Api\LampuController;
use App\Http\Controllers\Api\LaporanController;
use App\Http\Controllers\Api\SaranController;
use App\Http\Controllers\Api\SliderController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/laporan', [LaporanController::class, 'index']);
Route::get('/laporan/statistic/total', [LaporanController::class, 'getTotalGroupByMonth'])->name('laporan.totalMonth');
Route::get('/laporan/{id}', [LaporanController::class, 'show']);
Route::post('/laporan/{id}', [LaporanController::class, 'delete']);
Route::post('/laporan', [LaporanController::class, 'store']);
Route::get('/lampu', [LampuController::class, 'index']);
Route::get('/lampu-geojson', [LampuController::class, 'toGeometry'])->name('lampu.geometry');
Route::post('/saran', [SaranController::class, 'store']);
Route::get('/agenda', [AgendaController::class, 'index']);
Route::get('/agenda/{id}', [AgendaController::class, 'show']);
Route::get('/slider', [SliderController::class, 'index']);

